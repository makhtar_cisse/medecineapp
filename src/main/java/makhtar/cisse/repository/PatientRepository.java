package makhtar.cisse.repository;

import makhtar.cisse.domain.*;

public class PatientRepository {
    public Patient[] getAllPatientsPrincipal(){
        Hopital hopital = new Hopital(1,"Principal");
        return new Patient[]{
                new Patient(1,"Badiane","Alioune","07/10/1995",hopital),
                new Patient(2,"FALL","Abdoul","17/12/1996",hopital),
                new Patient(3,"SY","Amina","11/10/1993",hopital),
        };
    }
}
