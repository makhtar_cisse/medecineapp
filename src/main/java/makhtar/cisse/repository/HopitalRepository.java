package makhtar.cisse.repository;

import makhtar.cisse.domain.Hopital;

public class HopitalRepository{
    public Hopital[] getAll(){
        return new Hopital[]{
                new Hopital(1,"Principal"),
                new Hopital(2,"Dantec"),
                new Hopital(3,"Dallal Jamm"),
                new Hopital(4,"Fann"),
        };
    }
}
