package makhtar.cisse.service;

import java.util.Scanner;
import makhtar.cisse.repository.*;

import makhtar.cisse.domain.Hopital;
import makhtar.cisse.domain.Patient;

public class DisplayMenu {
    /**
     * MENU PRINCIPAL
     * */
    public void showWelcomeMenu(){

        boolean arret = false;
        while (!arret){

            System.out.println("Bienvenue sur la platforme des hopitaux du sénégal");
            System.out.println("1-Admin");
            System.out.println("2-Medecin");
            System.out.println("3-secretaire");
            System.out.println("4-Quitter");
            System.out.println("Veuillez choisir un profil pour continuer:");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            //clearConsole();
            switch (choice) {
                case 1:
                    showAdminMenu();
                    break;
                case 2:
                    //Menu Prof
                    break;
                case 3:
                    // Menu Parent
                    break;
                case 4: arret = true; break;

                default: System.out.println("entrez un choix entre 1 et 3"); break;

            }

        }

    }

    /**
     * Menu Admin
     */
    public void showAdminMenu(){
        int choice;
        HopitalRepository hopitalRepository = new HopitalRepository();
        PatientRepository patientRepository = new PatientRepository();
        Hopital[] hopitaux = hopitalRepository.getAll();

        do{
            System.out.println("Bienvenue Admin!!");
            System.out.println("1-Afficher les hopitaux");
            System.out.println("2-Afficher les patients");
            System.out.println("3-Quitter");
            System.out.println("Veuillez choisir une option pour continuer:");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            //clearConsole();
            switch (choice) {
                case 1:
                    System.out.println("Les hopitaux de votre plateformes:");
                    for (int i=0;i<hopitaux.length;i++){
                        Hopital hopital = hopitaux[i];
                        System.out.println(String.format("> %S %S", hopital.getId(),hopital.getLibelle()));
                    }
                    System.out.println("Choissisez un hopital pour voir les patients:");
                    int idHopital = scanner.nextInt();
                    if(idHopital == 1){
                        System.out.println("Les patients de l'hopital Principal:");
                        Patient[] patients = patientRepository.getAllPatientsPrincipal();
                        for (int i=0;i<patients.length;i++){
                            Patient patient = patients[i];
                            System.out.println(String.format("> %S %S", patient.getNom(),patient.getPrenom()));
                        }
                    }
                    break;
                case 2:
                    //
                    break;
                case 3:
                    //
                    break;

                default: System.out.println("entrez un choix entre 1 et 3"); break;

            }

        }while(choice!=3);

    }




    public final static void clearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows"))
            {
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            e.printStackTrace();
        }
    }
}

