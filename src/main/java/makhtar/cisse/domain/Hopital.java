package makhtar.cisse.domain;

public class Hopital {
    private int id;
    private String libelle;

    public Hopital() {
    }

    public Hopital(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
